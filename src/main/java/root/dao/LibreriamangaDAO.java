/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.dao.exceptions.NonexistentEntityException;
import root.dao.exceptions.PreexistingEntityException;
import root.persistence.entities.Libreriamanga;

/**
 *
 * @author javi3
 */
public class LibreriamangaDAO implements Serializable {

    public LibreriamangaDAO(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public LibreriamangaDAO() {
    }
    
    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Libreriamanga libreriamanga) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(libreriamanga);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findLibreriamanga(libreriamanga.getIdmanga()) != null) {
                throw new PreexistingEntityException("Libreriamanga " + libreriamanga + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Libreriamanga libreriamanga) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            libreriamanga = em.merge(libreriamanga);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = libreriamanga.getIdmanga();
                if (findLibreriamanga(id) == null) {
                    throw new NonexistentEntityException("The libreriamanga with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Libreriamanga libreriamanga;
            try {
                libreriamanga = em.getReference(Libreriamanga.class, id);
                libreriamanga.getIdmanga();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The libreriamanga with id " + id + " no longer exists.", enfe);
            }
            em.remove(libreriamanga);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Libreriamanga> findLibreriamangaEntities() {
        return findLibreriamangaEntities(true, -1, -1);
    }

    public List<Libreriamanga> findLibreriamangaEntities(int maxResults, int firstResult) {
        return findLibreriamangaEntities(false, maxResults, firstResult);
    }

    private List<Libreriamanga> findLibreriamangaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Libreriamanga.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Libreriamanga findLibreriamanga(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Libreriamanga.class, id);
        } finally {
            em.close();
        }
    }

    public int getLibreriamangaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Libreriamanga> rt = cq.from(Libreriamanga.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
