/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.persistence.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author javi3
 */
@Entity
@Table(name = "libreriamangas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Libreriamanga.findAll", query = "SELECT l FROM Libreriamanga l"),
    @NamedQuery(name = "Libreriamanga.findByIdmanga", query = "SELECT l FROM Libreriamanga l WHERE l.idmanga = :idmanga"),
    @NamedQuery(name = "Libreriamanga.findByNameman", query = "SELECT l FROM Libreriamanga l WHERE l.nameman = :nameman"),
    @NamedQuery(name = "Libreriamanga.findByTomoman", query = "SELECT l FROM Libreriamanga l WHERE l.tomoman = :tomoman"),
    @NamedQuery(name = "Libreriamanga.findByMangakaman", query = "SELECT l FROM Libreriamanga l WHERE l.mangakaman = :mangakaman"),
    @NamedQuery(name = "Libreriamanga.findByCantidadman", query = "SELECT l FROM Libreriamanga l WHERE l.cantidadman = :cantidadman")})
public class Libreriamanga implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idmanga")
    private Integer idmanga;
    @Size(max = 2147483647)
    @Column(name = "nameman")
    private String nameman;
    @Column(name = "tomoman")
    private Integer tomoman;
    @Size(max = 2147483647)
    @Column(name = "mangakaman")
    private String mangakaman;
    @Column(name = "cantidadman")
    private Integer cantidadman;

    public Libreriamanga() {
    }

    public Libreriamanga(Integer idmanga) {
        this.idmanga = idmanga;
    }

    public Integer getIdmanga() {
        return idmanga;
    }

    public void setIdmanga(Integer idmanga) {
        this.idmanga = idmanga;
    }

    public String getNameman() {
        return nameman;
    }

    public void setNameman(String nameman) {
        this.nameman = nameman;
    }

    public Integer getTomoman() {
        return tomoman;
    }

    public void setTomoman(Integer tomoman) {
        this.tomoman = tomoman;
    }

    public String getMangakaman() {
        return mangakaman;
    }

    public void setMangakaman(String mangakaman) {
        this.mangakaman = mangakaman;
    }

    public Integer getCantidadman() {
        return cantidadman;
    }

    public void setCantidadman(Integer cantidadman) {
        this.cantidadman = cantidadman;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idmanga != null ? idmanga.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Libreriamanga)) {
            return false;
        }
        Libreriamanga other = (Libreriamanga) object;
        if ((this.idmanga == null && other.idmanga != null) || (this.idmanga != null && !this.idmanga.equals(other.idmanga))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.persistence.entities.Libreriamanga[ idmanga=" + idmanga + " ]";
    }
    
}
