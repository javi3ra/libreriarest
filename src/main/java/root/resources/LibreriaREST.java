/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.resources;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.dao.LibreriamangaDAO;
import root.dao.exceptions.NonexistentEntityException;
import root.persistence.entities.Libreriamanga;

/**
 *
 * @author javi3
 */
@Path("/libreria")
public class LibreriaREST {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");
    EntityManager em;
    LibreriamangaDAO dao = new LibreriamangaDAO();

    // Listar todos los items de la BD
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarMangas() {
        List<Libreriamanga> lista = dao.findLibreriamangaEntities();
        return Response.ok(200).entity(lista).build();
    }

    //Buscar un item en la BD
    @GET
    @Path("/{iditembuscar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarId(@PathParam("iditembuscar") Integer idbuscar) {
        Libreriamanga cliente = dao.findLibreriamanga(idbuscar);
        return Response.ok(200).entity(cliente).build();
    }

    //Ingresar un item la BD
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public String nuevo(Libreriamanga itemNuevo) {
        try {
            dao.create(itemNuevo);
        } catch (Exception ex) {
            System.out.print("Algo ocurrio " + ex);
            Logger.getLogger(LibreriaREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "item guardado correctamente";
    }
    //Modificar un usuario en la BD

    @PUT
    public Response actualizar(Libreriamanga itemUpdate) {
        try { 
            dao.edit(itemUpdate);
        } catch (Exception ex) {
            System.out.print("Algo ocurrio -edit-" + ex);
            Logger.getLogger(LibreriaREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(itemUpdate).build();

    }
    //Eliminar item en la BD

    @DELETE
    @Path("/{iditemdelete}")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
    public Response eliminarId(@PathParam("iditemdelete") Integer iddelete) {
        try {     
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            System.out.print("Algo ocurrio -delete-" + ex);

            Logger.getLogger(LibreriaREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok("item borrado con Exito").build();
    }
}
