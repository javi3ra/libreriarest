<%-- 
    Document   : index
    Created on : 01-may-2020, 20:35:02
    Author     : javi3
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>LibreriaDB</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link rel="stylesheet" href="styles.css">
    </head>
    <body>   
        <font face="courier">
        <div id="1" style="margin-right: 250px; margin-left:  250px;" class="p-3 mb-2 bg-light">   
            <h4><small class="text-muted">Fernanda Anasco Seccion:50</small></h4>
            <h1><span class="badge badge-secondary">DB Libreria</span>   
                <p> <small class="text-muted">Metodos</small>
            </h1>   
            <div class="p-3 mb-2 bg-dark text-white">
                <h2><span class="badge badge-secondary">URL Server</span></h2>
                <input class="form-control" value="ttp://DESKTOP-T278FK5:8081/appREST-1.0-SNAPSHOT" readonly>
                <input class="form-control" value="https://appr3st.herokuapp.com" readonly>
            </div>  
            <h3><span class="badge badge-primary">Get</span></h3>
            <div class="input-group mb-3">         
                <div class="input-group-prepend">
                    <span class="badge badge-dark">Busque el item con el ID correspondiente</span>
                </div>
                <input type="text" class="form-control" name="urlget" value="/api/libreria/{iditembuscar}" readonly />
            </div>
            <p><h3><span class="badge badge-success">Post</span></h3>    
            <div class="input-group mb-3">         
                <div class="input-group-prepend">             
                    <span class="badge badge-dark">Ingrese un nuevo item con un ID correspondiente</span>
                </div>
                <input type="text" class="form-control" name="urlget" value="/api/libreria" readonly />
            </div>  
            <p><h3><span class="badge badge-info">Put</span></h3>    
            <div class="input-group mb-3">         
                <div class="input-group-prepend">             
                    <span class="badge badge-dark">Modifique el item con el ID correspondiente</span>
                </div>
                <input type="text" class="form-control" name="urlget" value="/api/libreria" readonly />
            </div>  
            <p><h3><span class="badge badge-dark">Delete</span></h3>        
            <div class="input-group mb-3">         
                <div class="input-group-prepend">             
                    <span class="badge badge-dark">Elimine el item con el ID correspondiente</span>
                </div>
                <input type="text" class="form-control" name="urlget" value="/api/libreria/{iditemdelete}" readonly />
            </div>  
        </div>
    </body>
</html>
